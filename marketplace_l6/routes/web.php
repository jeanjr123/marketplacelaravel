<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


                            //metodo all extendido da classe model dentro da model produto
  //Route::get('/model', function(){

    // $products = \App\Product::all(); //Select * from products (estava assim antes de colocar active record)
      //pela convencao se a classe e product vai procurar products como tabela
      //sempre no plural

      //Active Record = padrao de projeto, cria dados 

      //crio uma instancia de user 
              //passo o caminho todo pois nao importei
      // $user = new \App\User();
      // //o active record permite trabalhar as colunas do banco como se fossem atributos da class

      // $user->name = 'Usuario Teste';
      // $user->email= 'email@teste.com';
      // $user->password = bcrypt('12345678');

    // $user->save();

    //para atualizar
      //  $user = \App\User::find(1); //find busca pelo ID , find = metodo de busca do elouquent

      //  $user->name = 'Usuario Teste Atualizado ....';

      //  $user->save();

      //return \App\User::all(); retorna todos os usuarios
      //serializa = ele converte para json 
      //retorna uma colection que permite fazer diversas operacoes em uma colecao de dados

      //return \App\User::find(3); //retorna um unico usuario com base no id

      //return \App\User::where('name', 'Dr. Alfred Crona')->get(); //select * from users where name = nome
      //nesse caso mesmo oque retorne somente 1 ele retorna em colection
      //porque no caso de nome posso ter repetidos ao contrario de id

      //return \App\User::where('name', 'Dr. Alfred Crona')->first(); // limit 1

      // return \App\User::paginate(10); //traz dez por pagina de all() com link para outras paginas

      //Mass Assigment - Atribuicao em massa (inserirndo usuario)

      //o create espera o nome das colunas e valores como array associativo
      //passo um array que quero inserir na tabela users via model
      // $user = \App\User::create([   //esse create precisa que a model user tenha o atributo filable
      //     'name'  => 'Jean Junior Silva',
      //     'email' => 'jeanjr.silvasousa@gmail.com',
      //     'password' => bcrypt('123456')

      // ]);

      // Mass Update 

      // $user = \App\User::find(42);

      //$user = $user->update([
      //     'name' => 'Atualizando com mass update'
      // ]); //true or false vai retornar se eu atribuir o valor de user para outra variavel user

    // dd($user);

    //COMO EU FARIA PARA PEGAR A LOJA DE UM USUARIO

    // $user = \App\User::find(4);

  //  return $user->store;  //busca o objeto unico store pois a relacao e 1:1
    //se for N:N volta uma colection de dados


    //dd($user->store()->count()); //se fazer como metodo retorna uma instancia de hasone
      //conto quantas lojas o usuario tem


      //PEGAR OS PRODUTOS DE UMA LOJA

      //$loja = \App\Store::find(1);

      //return $loja->products; //retorna uma collection

      // return $loja->products()->count(); //consigo fazer o count a partir do atributo
      //pois o atributo produto ja retorna uma collection


      //PEGAR SOMENTE PRODUTOS DE ID 1 DA LOJA

    //  return $loja->products()->where('id', 1)->get();
      //quando imprime como metodo tem mais possibilidades de dar tratativas na collection

      // dd($loja->products());
    // return \App\User::all(); 

    //PEGAR AS CATEGORIAS DE UMA LOJA

      //$categoria = \App\Category::find(1);

      // return $categoria->products;   // traria todos os produtos da categoria de id 1
      //retorna colecao de dados N:N

  //});

//1 parametro url principal, 2 funcao anonima callback
Route::get('/', 'HomeController@index')->name('home');


Route::get('/product/{slug}', 'HomeController@single')->name('product.single');

Route::get('/category/{slug}', 'CategoryController@index')->name('category.single');

Route::get('/store/{slug}', 'StoreController@index')->name('store.single');

Route::prefix('cart')->name('cart.')->group(function () {

     Route::get('/','CartController@index')->name('index'); 

     Route::post('add', 'CartController@add')->name('add');

     Route::get('remove/{slug}', 'CartController@remove')->name('remove');

     Route::get('cancel', 'CartController@cancel')->name('cancel');

});

Route::prefix('checkout')->name('checkout.')->group(function(){
  
     Route::get('/', 'CheckoutController@index')->name('index');

     Route::post('/process', 'CheckoutController@proccess')->name('proccess');

     Route::get('/thanks', 'CheckoutController@thanks')->name('thanks');

});

Route::get('/model', function(){

      //Criar uma loja para um usuario


  //$user = \App\User::find(10);

                    //vai criar uma loja e receber a referencia do usuario de id 10
                    //cria uma store a partir do relacionamento
  //    $store = $user->store()->create([    //create retorna o objeto com id que recebeu do banco de dados
  //         'name' => 'Loja Teste',
  //         'description' => 'Loja teste de produtos de informatica',
  //         'mobile_phone' => 'XX-XXXX-XXXX',
  //         'phone' => 'XX-XXXX-XXXX',
  //         'slug' => 'loja-teste'
  //     ]);

    //  dd($store);

      

      //Criar um produto para uma loja

  //     $store = \App\Store::find(41);

  //   $product =  $store->products()->create([
  //         'name' => 'Notebook Dell' ,
  //         'description' => 'Core i5 10GB',
  //         'body' => 'Quaquer Coisa',
  //         'price' => 2999.90,
  //         'slug' => 'notebook-dell'
  //     ]);


  //     dd($product);



    //Criar uma categoria

    // \App\Category::create([
    //     'name' => 'Games',
    //     'description' => null,
    //     'slug' => 'games'
    // ]);


    // \App\Category::create([
    //     'name' => 'Notebooks',
    //     'description' => null,
    //     'slug' => 'notebooks'
    // ]);



    //  return \App\Category::all();


    //Adicionar um produto para uma categoria

    $product = \App\Product::find(45);
                //attach adiciona ao produto a categoria de id 1
                //adicionou na tabela category_product
                //para remover use o dettach
  //  dd($product->categories()->attach([1]));

  //  dd($product->categories()->detach([1]));

  //para adicionar mais de uma categoria ao produto use o sync

  //  dd($product->categories()->sync([1,2]));

  // para remover basta tirar o id da categoria  do array
 // dd($product->categories()->sync([2]));

     return $product->categories;   
});

//no laravel responde para as rotas get(recupera), post(cria), put(atualiza) 
//, patch(atualiza), delete, options(dentro do http retorna quais cabecalho a rota especifica responde)


                            //chamando o metodo de um controller no segundo parametro
                            //no exemplo chamo o store controller, coloco duas barras para escapar uma
                            //o metodo chamo com @nomemetodo
                            //para chegar na pasta controller o laravel faz automatico
// Route::get('/admin/stores', 'Admin\\StoreController@index');

//criacao de uma loja
// Route::get('/admin/stores/create', 'Admin\\StoreController@create');

//recebendo dados do formulario
// Route::post('/admin/stores/store', 'Admin\\StoreController@store');


//O admin do inicio da rota ja que esta repetido pode ser substituido pelo prefix
//posso substituir o segundo admin pelo namespace

// Route::prefix('admin')->namespace('Admin')->group(function(){

//   Route::prefix('stores')->group(function(){
//                                          //name = definir um alias para a rota 
//     Route::get('/', 'StoreController@index')->name('admin.stores.index');
  
//     Route::get('/create', 'StoreController@create')->name('admin.stores.create');
  
//     Route::post('/store', 'StoreController@store')->name('admin.stores.store');
//            //parametro dinamico store          //aponta para o metodo edit
//     Route::get('/{store}/edit', 'StoreController@edit')->name('admin.stores.edit');
//                           //store chega no controller como $store
//     Route::post('/update/{store}', 'StoreController@update')->name('admin.stores.update');
//           //get pois vai via url
//     Route::get('/destroy/{store}/', 'StoreController@destroy')->name('admin.stores.destroy');

//   });


// });






//abaixo estou definindo middlewares para um grupo de rotas
//o primeiro parametro sao as middlewares e o segundo a function anonima callback

Route::group(['middleware' => ['auth']], function(){

        //posso colocar mais um name e simplificar

    Route::prefix('admin')->name('admin.')->namespace('Admin')->group(function(){

      // Route::prefix('stores')->name('stores.')->group(function(){
        //                                        //name = definir um alias para a rota 
        //   Route::get('/', 'StoreController@index')->name('index');
        
        //   Route::get('/create', 'StoreController@create')->name('create');
        
        //   Route::post('/store', 'StoreController@store')->name('store');
        //          //parametro dinamico store          //aponta para o metodo edit
        //   Route::get('/{store}/edit', 'StoreController@edit')->name('edit');
        //                         //store chega no controller como $store
        //   Route::post('/update/{store}', 'StoreController@update')->name('update');
        //         //get pois vai via url
        //   Route::get('/destroy/{store}/', 'StoreController@destroy')->name('destroy');

      // });
      
      //rotas de controllers como recursos

      Route::resource('stores', 'StoreController');

      Route::resource('products', 'ProductController');

      Route::resource('categories','CategoryController');

      Route::post('photos/remove', 'ProductPhotoController@removePhoto')->name('photo.remove');




});

   
    
   

});






Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');//->middleware('auth');
