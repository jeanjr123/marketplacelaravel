<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //$categories = \App\Category::all(['name','slug']);
        //view share é utilizado para compartilhar informações para todas as views
        //sendo view()->share('chave a ser compartilhada', 'valor')
        //view()->share('categories', $categories);

        //já o view composer passo as views em que quero compartilhar a informação
        //posso por exemplo compartilhar com todas as views o conteudo de um func anonima

        // view()->composer('*', function($view) use ($categories){
        //    //view = uma instancia da view welcome
        //    $view->with('categories', $categories);
        // });

        //no exemplo o primeiro parametro são todas as views
        //o segundo parametro é o que vai ser executado
        view()->composer('*', 'App\Http\Views\CategoryViewComposer@compose');

    }
}
