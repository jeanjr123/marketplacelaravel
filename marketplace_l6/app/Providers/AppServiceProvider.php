<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services. starta qualquer serviço
     *
     * @return void
     */
    public function boot()
    {
        //inicializa as configurações globalmente na aplicação para o uso do sdk
        \PagSeguro\Library::initialize();
        \PagSeguro\Library::cmsVersion()->setName("Marketplace")->setRelease("1.0.0");
        \PagSeguro\Library::moduleVersion()->setName("Marketplace")->setRelease("1.0.0");

        $categories = \App\Category::all(['name','slug']);
        //view share é utilizado para compartilhar informações para todas as views
        //sendo view()->share('chave a ser compartilhada', 'valor')
        //view()->share('categories', $categories);

        //já o view composer passo as views em que quero compartilhar a informação
        //posso por exemplo compartilhar com todas as views o conteudo de um func anonima

        // view()->composer('*', function($view) use ($categories){
        //    //view = uma instancia da view welcome
        //    $view->with('categories', $categories);
        // });

        view()->composer('*', 'App\Http\Views\CategoryViewComposer@compose');


    }
}
