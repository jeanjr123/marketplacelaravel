<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

//autenticatable = traz metodos para trabalhar autenticacao para o usuario
class User extends Authenticatable
{
    use Notifiable; //sistema de notificacao via email, sms, push

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // o atributo fillable mostra quais campos posso passar valores quando usar 
    // MASS assigment
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    //remove / esconde o campo da listagem
    protected $hidden = [
        'password', 'remember_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */

    //casts converte automaticamente o campo para o valor desejado
    //exemplo se colocar nome igual a boolean e buscar todos os users ,
    // no lugar do nome ele ira trazer TRUE
    protected $casts = [
        'email_verified_at' => 'datetime'

    ];

    //preciso criar um metodo que representa a ligacao , a funcao vai retornar esse metodo
    //ex: representar que um ususario tem uma loja
    public function store()
    {
        //usuario has one (tem uma) loja
        return $this->hasOne(Store::class);
        //no caso do model que carrega hasOne ele usa o nome da classe_id
        //ou seja na ((tabela store)) tem que ter user_id ou seja olha a foreign key
    }

    public function orders()
    {
      return $this->hasMany(UserOrder::class);
    }
}


//Representacoes de ligacoes no elouquent

// 1:1 (um para um usuario(has one - tem uma loja) e loja)  | hasOne e belongsTo
// 1:N (um para n  loja(hasMany - tem muitos) e produtos(belongsTo - pretence para )) | hasMany e belongsTo
// N:N (N para N produtos e categorias) | belongsToMany


// um usuario has one loja e uma loja belongsto usuario
// uma loja has many produtos e um produto belongs to loja
// um produto belongstomany produtos (pertence a muitos)