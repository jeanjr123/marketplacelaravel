<?php

namespace App\Traits;

trait UploadTrait
{
    private function imageUpload($images, $imageColumn = null){
       

        $uploadedImages = [];

        if(is_array($images)){


            foreach($images as $image){
                //store é o método que salva
                //primeiro parametro de store é a pasta que vou salvar a img
                //o segundo parametro é o disco que vou usar 
                //disco que se encontra em filesystems 
                //vai criar uma pasta que se chama products dentro do caminho do disc public
                // caminho: storage/app/public/products
        
                    $uploadedImages[] = [$imageColumn => $image->store('products', 'public')];
            }
        }else{

            $uploadedImages = $images->store('logo','public');
        }

     

        return $uploadedImages;
    }
}
