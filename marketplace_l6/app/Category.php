<?php

namespace App;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

use Illuminate\Database\Eloquent\Model;


class Category extends Model
{                        
    use HasSlug;
    
    protected $fillable = ['name', 'description', 'slug'];



       /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }
        
    //Vai procurar a chave estrangeira na tabela category_product
                         // o nome da mistura dos dois models em ordem alfabetica
    public function products(){
                                                   //nome da tabela associativa
        return $this->belongsToMany(Product::class, 'category_product');
    }
}
