<?php

namespace App;

use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Illuminate\Database\Eloquent\Model;
//a classe store extende o model do elouquent
// ORM = traduz uma base relacional para um ponto orientado a objetos, ou seja models
// o elouquent serve para essa traducao de relacional a orientado a objetos
class Store extends Model
{
    use HasSlug;

    protected $fillable = [
        'name', 'description', 'phone',
        'mobile_phone', 'slug', 'logo'
    ];


    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
        //na tabela store ele ira procurar o campo user_id 
        //se nao tiver esse nome eu coloco os outros parametros
    }

    public function products()
    {
        //loja tem muitos produtos
        return $this->hasMany(Product::class);
        //como carrega has many na tabela product vai procurar store id
    }


    public function orders()
    {
        $this->hasMany(UserOrder::class);
    }
}

//O elouquent funciona da seguinte forma
//ele procura sempre a class Store no banco de dados como stores
//caso seja diferente user protectes $variavel(atributo) = 'nome da tabela' 