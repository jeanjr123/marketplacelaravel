<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
              
             //a classe do produto extende o model do elouquent
             // ORM = traduz uma base relacional para um ponto orientado a objetos, ou seja models
             // o elouquent serve para essa traducao de relacional a orientado a objetos
class Product extends Model
{
    use HasSlug;

    protected $fillable = ['name', 'description', 'body', 'price',
    'slug'];




       /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }
    
    public function store(){

      return $this->belongsTo(Store::class);
        //na tabela product ele vai procurar store_id
    }

    public function categories(){

      return $this->belongsToMany(Category::class, 'category_product');
        
    }

    public function photos(){

      return $this->hasMany(ProductPhoto::class);

    }
}
