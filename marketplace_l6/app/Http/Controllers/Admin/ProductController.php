<?php

//comando de geracao desse controller ->  php artisan make:controller Admin/ProductController --resource
//resource = recursos , ou seja esse controller e um controller como recurso
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;
use App\Http\Requests\ProductRequest;
use GuzzleHttp\Promise\AggregateException;
use App\Traits\UploadTrait;

class ProductController extends Controller
{
    use UploadTrait;
    private $product; //atributo privado

    //definindo o construtor
    //O parametro esta tipado como Product 
    //entao quando eu instanciar o productController
    //estarei instanciando new Product que e- o model
    public function __construct(Product $product)
    { //injecao da instancia do produto

        $this->product = $product; //atributo privado recebe o valor do parametro
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    //index lista meus produtos
    public function index()
    {
        //quando acesso com o this estou acessando o atributo privado product
        //no entanto minha funcao construtor faz que ele receba o valor produtct
        //que tem o tipo da model product, ou seja 'e como se eu instanciasse product
        //nao sendo necessario chamar \App\product

        $userStore = auth()->user()->store;

        $products = $userStore->products()->paginate(10);

        return view('admin.products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    //serve para exibir formulario de criacao
    public function create()
    {
        $categories = \App\Category::all(['id', 'name']);

        return view('admin.products.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    //faz o processamento da criacao
    //o create submete para ca
    public function store(ProductRequest $request)
    {


        $data = $request->all();

        $categories = $request->get('categories', null);
        //pega loja de usuario autenticado
        $store = auth()->user()->store;

        //cria produto para a loja de acordo com o seu relacionamento
        $product = $store->products()->create($data);
        //criando todas as categorias passadas no multiple selct /array
        //para o produto
        $product->categories()->sync($categories);

        //se existir fotos na requisição
        if ($request->hasFile('photos')) {
            //images vai receber o return do metodo privado imageUpload
            $images = $this->imageUpload($request->file('photos'), 'image');

            //inserção destas imagens (ou seja a referencia da imagem) no banco de dados
            $product->photos()->createMany($images);
        }



        flash('Produto Criado Com Sucesso!')->success();
        //redireciona para rota deste apelido
        return redirect()->route('admin.products.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    //para criar uma visualizacao rapida de um dado
    public function show($id)
    {
        //return $id;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $product
     * @return \Illuminate\Http\Response
     */

    //exibir formulario de edicao
    public function edit($product)
    {
        $product = $this->product->findOrFail($product);

        $categories = \App\Category::all(['id', 'name']);

        return view('admin.products.edit', compact('product', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $product
     * @return \Illuminate\Http\Response
     */

    //processamento da atualizacao
    public function update(ProductRequest $request, $product)
    {
        $data = $request->all();

        $categories = $request->get('categories', null);

        $product = $this->product->find($product);

        $product->update($data);

        if (!is_null($categories)) {
            $product->categories()->sync($categories);
        }

        //se existir fotos na requisição
        if ($request->hasFile('photos')) {
            //images vai receber o return do metodo privado imageUpload
            $images = $this->imageUpload($request->file('photos'), 'image');

            //inserção destas imagens (ou seja a referencia da imagem) no banco de dados
            $product->photos()->createMany($images);
        }

        flash('Produto Atualizado Com Sucesso!')->success();

        return redirect()->route('admin.products.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $product
     * @return \Illuminate\Http\Response
     */
    //metodo para remocao
    public function destroy($product)
    {
        //para cair nesse metodo de remocao a requisicao tem que ser 
        //do tipo Delete
        $product = $this->product->find($product);

        $product->delete();

        flash('Produto Removido com Sucesso!')->success();

        return redirect()->route('admin.products.index');
    }


}
