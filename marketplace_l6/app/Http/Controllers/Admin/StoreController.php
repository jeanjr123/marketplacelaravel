<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\StoreRequest;
use App\Traits\UploadTrait;
use Illuminate\Support\Facades\Storage;

class StoreController extends Controller
{
    use UploadTrait;

    public function __construct()
    {
        //only vai executar a middleware apenas para os metodos 
        //citados
        $this->middleware('user.has.store')->only(['create', 'store']);
    }



    public function index()
    {

        $store = auth()->user()->store;



        //vou retornar uma view, que esta na pasta views
        // admin, stores que se chama index
        //compact esta mandando a variavel stores para a view
        return view('admin.stores.index', compact('store'));
    }

    public function create()
    {
        //se ja tiver loja eu redireciono para o index





        //pegar o id e nome de todos os usuarios para carregar na view

        $users = \App\User::all(['id', 'name']);

        //caminho da view       //passa users para view
        return view('admin.stores.create', compact('users'));
    }


    //esse metodo ira processar a criacao
    //vai receber dados da request ou seja dados dos formularios
    //passso como parametro como o tipo Request que foi importado la em cima 
    //o laravel resolve e injeta a instancia do request 
    public function store(StoreRequest $request)
    {

        //se ja tiver loja eu redireciono para o index


        //pega todos os dados que vier na instancia request
        //die and dumpp
        $data = $request->all();

        $user = auth()->user();

        if ($request->hasFile('logo')) {
            //image upload = metodo da trait
            $data['logo'] = $this->imageUpload($request->file('logo'));
        }


        //$user = \App\User::find($data['user']);

        //crio loja
        $store = $user->store()->create($data);

        flash('Loja Criada Com Sucesso')->success();

        return redirect()->route('admin.stores.index');
    }

    //o que vier na url passa como parametro
    public function edit($store)
    {

        $store = \App\Store::find($store);

        return view('admin.stores.edit', compact('store'));
    }      //1 param request, 2 param id da loja



    public function update(StoreRequest $request, $store)
    {

        $data = $request->all();

        $store = \App\Store::find($store);


        if ($request->hasFile('logo')) {
            if(Storage::disk('public')->exists($store->logo)){
                Storage::disk('public')->delete($store->logo);
            }
            //image upload = metodo da trait
            $data['logo'] = $this->imageUpload($request->file('logo'));
        }

        $store->update($data);

        //mensagen vai ser passada para o flash que esta em app.blade 

        flash('Loja Atualizada Com Sucesso')->success();
        //redirecionar para route = usar o apelido da rota
        return redirect()->route('admin.stores.index');
    }

    //parametro dinamico que veio da url
    public function destroy($store)
    {

        $store = \App\Store::find($store);

        $store->delete();

        flash('Loja Removida Com Sucesso')->success();

        //retorna a rota que quer redirecionar
        return redirect()->route('admin.stores.index');
    }
}
