<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\ProductPhoto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProductPhotoController extends Controller
{
    public function removePhoto(Request $request)
    {
        $photoName = $request->get('photoName');


        //Removo dos arquivos
        //A classe Storage no laravel gerencia o armazenamento de arquivos
        if (Storage::disk('public')->exists($photoName)) {

            Storage::disk('public')->delete($photoName);
        }

        //Remove a imagem do banco

        $removePhoto = ProductPhoto::where('image', $photoName);
        //acessa o valor da coluna product id como se fosse objeto
        $product_id = $removePhoto->first()->product_id;

        $removePhoto->delete();

        flash('Imagem removida com sucesso')->success();
        //chamando a rota edit para depois que remover recarregar pagina
        //com o mesmo produto
        return redirect()->route('admin.products.edit', ['product' => $product_id]);
    }
}
