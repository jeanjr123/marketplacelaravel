<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    private $product;

    public function __construct(Product $product)
    {

        $this->product = $product; //atributo privado recebe o valor do parametro
    }


    public function index()
    {
        $products = $this->product->limit(6)->orderBy('id','desc')->get();

        $stores = \App\Store::limit(3)->orderBy('id','desc')->get();

        return view('welcome', compact('products', 'stores'));
    }

    public function single($slug)
    {
        $product = $this->product->whereSlug($slug)->first();

        return view('single', compact('product'));
    }
}
