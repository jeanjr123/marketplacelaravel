<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CartController extends Controller
{

    public function index()
    {
        $cart = session()->has('cart') ? session()->get('cart') : [];

        return view('cart', compact('cart'));

    }


    public function add(Request $request)
    {
        $productData = $request->get('product');

        $product = \App\Product::whereSlug($productData['slug']);

        //segurança para não mexer no hidden
        if(!$product->count() || $productData['amount'] == 0) 
        return redirect()->route('home');

         $product = array_merge($productData, $product->first(['name','price'])->toArray());

        //verificar se existe sessão para os produtos
        //se nao passar parametro retorna uma instancia da sessao e vou verificar se existe a
        //chave cart
         if (session()->has('cart')) {

            $products = session()->get('cart');

            $productsSlugs = array_column($products, 'slug');

            //se dentro da sessão já tiver o produto que estou adicionando no carrinho
            //vou chamar a funcão que itera o amount ao inves de adicionar outro product ao cart
            if (in_array($product['slug'], $productsSlugs)) {
                $products = $this->productIncrement($product['slug'], $product['amount'], $products);

                session()->put('cart', $products);
            }else{

                //existindo eu adiciono este produto na sessao existente
                session()->push('cart', $product);
            }


         }else {
             //nao existindo eu crio esta sessao com primeiro produto
             $products[] = $product;

             session()->put('cart', $products);
         }


         flash('Produto Adicionado no carrinho!')->success();

         return redirect()->route('product.single', ['slug' => $product['slug']]);

    }

    public function remove($slug)
    {
        if (!session()->has('cart')) {
            return redirect()->route('cart.index');
        }

        $products = session()->get('cart');
        //array filter recebe dois parametros, o array 
        //e uma funcao de callback que vai retornar a cada linha os produtos
        //que não seja o produto do slug recebido
        $products = array_filter($products, function ($line) use ($slug) {
            return $line['slug'] != $slug;
        });

        //session vai ter o array cart substituido pelo novo array
        session()->put('cart', $products);

        return redirect()->route('cart.index');
    }

    public function cancel()
    {
        //forget tira essa chave da sessão
        session()->forget('cart');

        flash('Desistencia da compra realizada com sucesso!')->success();

        return redirect()->route('cart.index');
 

    }

    private function productIncrement($slug, $amount, $products)
    {                        //funcao anonima verifica qual linha do array
        //da sessao de produtos tem o slug igual o que esta vindo na requisicao
        //pego o amount dessa linha e somo o valor vindo da requisição
        $products = array_map(function($line) use($slug, $amount){
             if($slug == $line['slug']) {
                 $line['amount'] += $amount;
             }

             return $line;
        }, $products);

        return $products;
    }
}
