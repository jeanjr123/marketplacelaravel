<?php

namespace App\Payment\PagSeguro;

class CreditCard
{
    private $items;

    private $user;

    private $cardInfo;

    private $reference;

    public function __construct($items, $user, $cardInfo, $reference)
    {
        $this->items = $items;
        $this->user = $user;
        $this->cardInfo = $cardInfo;
        $this->reference = $reference;
    }

    public function doPayment()
    {

             //reference = referencia para identificar a transaçao futuramente
             //$reference =   'XPTO';

             //primeiro passo chamamos do sdk o objeto cartao de credito instanciado
             $creditCard = new \PagSeguro\Domains\Requests\DirectPayment\CreditCard();
     
             //o nosso email como recebedores do pagamento
                               //com o helper env do laravel posso pegar a variavel de ambiente
             $creditCard->setReceiverEmail(env('PAGSEGURO_EMAIL'));
     
             // Set a reference code for this payment request. It is useful to identify this payment
             // in future notifications.
             $creditCard->setReference($this->reference);
     
             // Set the currency
             $creditCard->setCurrency("BRL");
     

             foreach($this->items as $item){
                   // Add an item for this payment request
                 $creditCard->addItems()->withParameters(
                     $this->reference,
                     $item['name'],
                     $item['amount'],
                     //passa somente o preço singular e o pag seguro soma
                     $item['price'],
                 );
             }
     
     
        
           
             //informacoes do cliente
             // Set your customer information.
             // If you using SANDBOX you must use an email @sandbox.pagseguro.com.br
      
             $user = $this->user;
             $email = env('PAGSEGURO_ENV') == 'sandbox' ? 'test@sandbox.pagseguro.com.br' : $user->email;
     
     
             $creditCard->setSender()->setName($user->name);
             $creditCard->setSender()->setEmail($email);
     
             $creditCard->setSender()->setPhone()->withParameters(
                 11,
                 56273440
             );
     
             $creditCard->setSender()->setDocument()->withParameters(
                 'CPF',
                 '41092909885'
             );
     
             $creditCard->setSender()->setHash($this->cardInfo['hash']);
     
             $creditCard->setSender()->setIp('127.0.0.0');
     
             // Set shipping information for this payment request
             $creditCard->setShipping()->setAddress()->withParameters(
                 'Av. Brig. Faria Lima',
                 '1384',
                 'Jardim Paulistano',
                 '01452002',
                 'São Paulo',
                 'SP',
                 'BRA',
                 'apto. 114'
             );
     
             //Set billing information for credit card
             $creditCard->setBilling()->setAddress()->withParameters(
                 'Av. Brig. Faria Lima',
                 '1384',
                 'Jardim Paulistano',
                 '01452002',
                 'São Paulo',
                 'SP',
                 'BRA',
                 'apto. 114'
             );
     
             // Set credit card token
             $creditCard->setToken($this->cardInfo['card_token']);
                                                           //essa chave recebe a quantidade que a 
                                                           //compra foi dividida e o valor das 
                                                           //parcelas
             list($quantity, $installmentAmount) = explode('|', $this->cardInfo['installment']);
                                     //number format aqui é para apenas garantir duas casas decimais
             $installmentAmount = number_format($installmentAmount, 2, '.', '');
             // Set the installment quantity and value (could be obtained using the Installments
             // service, that have an example here in \public\getInstallments.php)
             $creditCard->setInstallment()->withParameters($quantity, $installmentAmount);
     
             // Set the credit card holder information
             $creditCard->setHolder()->setBirthdate('01/10/1979');
             $creditCard->setHolder()->setName($this->cardInfo['card_name']); // Equals in Credit Card
     
             $creditCard->setHolder()->setPhone()->withParameters(
                 11,
                 56273440
             );
     
             $creditCard->setHolder()->setDocument()->withParameters(
                 'CPF',
                 '41092909885'
             );
     
             // Set the Payment Mode for this payment request
             $creditCard->setMode('DEFAULT');
     
             // Set a reference code for this payment request. It is useful to identify this payment
             // in future notifications.
     
             $result = $creditCard->register(
                 \PagSeguro\Configuration\Configure::getAccountCredentials()
             );


             return $result;

    }
}