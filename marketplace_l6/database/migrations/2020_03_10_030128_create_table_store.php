<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableStore extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */  //$table->tipoColuna('nomecoluna')
    public function up()
    {   
        Schema::create('stores', function (Blueprint $table) {
            $table->bigIncrements('id'); //ja passa o segundo parametro de unsigned como true = auto increment
            $table->unsignedBigInteger('user_id'); //unsigned big integer pois e um inteiro e sem true porque 
            //nao e auto increment      
            $table->string('name');
            $table->string('description');
            $table->string('phone');
            $table->string('mobile_phone');
            $table->string('slug');
                                 //referenciando user na tabela store
              //definindo a chave estrangeira para a coluna user_id
                    //chave estrangeira, refere-se a coluna id, onde = tabela users
            $table->foreign('user_id')->references('id')->on('users'); //nome da foreign key 
                    // stores_users_id_foreign


            $table->timestamps(); //Usuarios (users) -> lojas (stores)
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stores');
    }
}
