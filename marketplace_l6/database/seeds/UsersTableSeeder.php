<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */   //Para executar php artisan db:seed
    public function run()
    {
            //DB = classe do laravel, depois do insert passo array com colunas que quero inserir
        //    \DB::table('users')->insert(
        //       [
        //         'name' => 'Administrador',
        //         'email' => 'admin@admin.com',
        //         'email_verified_at' => now(),
        //         'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        //         'remember_token' => 'okokokokokk'
        //       ]
        //       );

            //passa o caminho da classe que ira criar
            //o segundo parametro e a quantidade
                                                //each serve para cada criacao executo uma acao 
        factory(\App\User::class, 40)->create()->each(
            //user do parametro vai conter o usuario criado 
            function($user){
                //o metodo store dentro de user vai criar uma loja que tem a ligacao, usando save
                //sempre uso save para objetos e create para arrays
                //save alem de criar vai pegar o id do user
                //o user id da loja vai ser preenchido com o id do usuario
                $user->store()->save(factory(\App\Store::class)->make()); //make cria uma store
                                                                           //com info fake da factory


            }
        );
    }
}
