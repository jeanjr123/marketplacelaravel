<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    //As Seeders permite que criemos dados para nossas tabelas
    //para fazermos testes
    //para criar php artisan make:seeder UsersTableSeeder
    public function run()
    {
         $this->call(UsersTableSeeder::class);
    }
}
