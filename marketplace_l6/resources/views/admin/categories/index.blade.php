@extends('layouts.app')


@section('content')

<a href="{{route('admin.categories.create')}}" type="button" class="btn btn-lg btn-success" 
style='margin-bottom: 2% '>Nova Categoria</a>

<table class="table table-striped table-hover">
    <thead>
        <th>id</th>
        <th>Nome</th>
        <th>Descricão</th>
        <th>Ações</th>
    </thead>
    <tbody>
        @foreach ($categories as $category)
        <tr>
            <td>{{$category->id}}</td>
            <td>{{$category->name}}</td>
            <td>{{$category->description}}</td>
            <td>
               <div class="btn-group">
                   <a href="{{route('admin.categories.edit',['category' => $category->id])}}" 
                    type="button" class="btn btn-primary btn-sm">Editar</a>
                </div>

                <div class="btn-group">
                <form action="{{route('admin.categories.destroy', ['category' => $category->id])}}"
                     method="post">
                    @csrf 
                    @method('delete')
                        <button class="btn btn-danger btn-sm" type="submit">Excluir</button>
                    </form> 
                </div>  
            
                
            </td>
        </tr>        
        @endforeach
    </tbody>
</table>

{{$categories->links()}}

@endsection