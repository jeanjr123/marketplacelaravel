@extends('layouts.app')

@section('content')
    
<form action="{{route('admin.categories.update', ['category' => $category->id])}}" method="post">
    @csrf
    @method('put')
    <div class="form-group">
        <label>Nome</label>
       <input type="text" name="name" value="{{$category->name}}" class="form-control">
    </div>

    <div class="form-group">
        <label>Descrição</label>
    <input type="text" name="description" value="{{$category->description}}" class="form-control">
    </div>
    
    <div class="buttom-group">
       <button type="submit" class="btn btn-lg btn-success">Atualizar</button>
    </div>

    </form>
@endsection