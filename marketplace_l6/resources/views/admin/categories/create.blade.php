@extends('layouts.app')

@section('content')
    
<form action="{{route('admin.categories.store')}}" method="post">
    @csrf

    <div class="form-group">
        <label>Nome</label>
    <input type="text" name="name" value="{{old('name')}}" class="form-control 
        @error('name') is-invalid   @enderror">
        @error('name')
           <div class="invalid-feedback">
               {{$message}}
           </div>
        @enderror
    </div>

    <div class="form-group">
        <label>Descrição</label>
    <input type="text" name="description" value="{{old('description')}}" class="form-control 
        @error('description') is-invalid  @enderror">
        @error('description')
            <div class="invalid-feedback">
                {{$message}}
            </div>
        @enderror
    </div>

    <div class="buttom-group">
       <button type="submit" class="btn btn-lg btn-success">Cadastrar</button>
    </div>

    </form>
@endsection