<!-- coloco a diretiva extends para colocar o conteudo dentro de yield 
vou substituir dentro de layouts/app -->
@extends('layouts.app')
    
 <!-- O parametro dentro da diretiva section e onde irei adicionar -->
@section('content')

<h1>Atualizar Produto</h1>
<!-- esse route = uma funcao que espera o alias ou seja o apelido da rota definido
    com name no web dentro de routees, quando chamo o create esse form vai submeter
para store onde e criado o produto =  update = nome do metodo de atualizacao -->  


<form action="{{route('admin.products.update', ['product' => $product->id])}}" method="post" enctype="multipart/form-data">


<!-- O laravel precisa saber que a requisicao vem da propria aplicacao por isso
uso o csrf token  
<input type="hidden" name="_token" value="{{csrf_token()}}">  -->
<!-- o input pode ser substituido por -->
  @csrf <!-- POR DENTRO ELE TRATA COMO UM INPUT VIA HIDDEN COM
O NOME _token E O VALUE {{csrf_token()}} -->

<!-- Consigo fazer o formulario suportar mais verbos http do que get e post
passando via hidden o input com nome _method e o value = verbo http que deseja 

<input type="hidden" name="_method" value="PUT">

mas pode ser substituido por aroba method

-->
 @method("PUT") <!-- por dentro ele trada como um input via hidden com
o name _method e o value PUT -->

<div class="form-group">
    <label>Nome Produto</label>
    <input type="text" name="name" class="form-control" value="{{$product->name}}">
</div>

<div class="form-group">
    <label>Descricao</label>
    <input type="text" name="description" class="form-control" value="{{$product->description}}">
</div>

<div class="form-group">
    <label>Conteudo</label>
    <textarea name="body" id="" cols="30" rows="10" class="form-control" >{{$product->body}}</textarea>
</div>

<div class="form-group">
    <label>Preco</label>
    <input type="text" name="price" class="form-control" value="{{$product->price}}">
</div>

<div class="form-group">
    <label>Categorias</label>
     <!-- Name = categories com notacao de array  -->
    <select name="categories[]" id="" 
     class="form-control @error('categories.*') is-invalid @enderror" multiple>
        @foreach ($categories as $category)
             <option value="{{$category->id}}"
                @if($product->categories->contains($category)) selected @endif
                >{{$category->name}}</option>
        @endforeach
    </select>
    @error('categories.*')  
    <div class="invalid-feedback">
        {{$message}}
    </div>
    @enderror
</div>

<div class="form-group">
    <label>Fotos do Produto</label>
    <input class="form-control @error('photos.*') is-invalid  @enderror"
     type="file" name="photos[]" multiple>
    @error('photos.*')
    <div class="invalid-feedback">
       {{$message}}
    </div>
    @enderror
</div>


<div>
    <button class="btn btn-lg btn-success" type="submit">Atualizar Produto</button>
</div>


</form>

<hr>

<div class="row">
    @foreach($product->photos as $photo)
        <div  class="col-md-4 text-center">
            <!-- SABENDO QUE O LARAVEL SO TEM ACESSO A PASTA PUBLIC E NÃO A STORAGE
                 EU CRIO UM LINK SIMBOLICO ONDE O LARAVEL CRIARÁ UMA PASTA STORAGE 
                 DENTRO DA PASTA PUBLICO COM OS CONTEUDOS DA STORAGE 
                 FAÇO ISSO COM O COMANDO 
                 "php artisan storage:link"
                abaixo estou acessando a pasta storage porque ela esta dentro de public
                pois o asset aponta para o public -->

            <img src="{{asset('storage/' . $photo->image )}}" alt="" class="img-fluid">

             <form action="{{route('admin.photo.remove')}}" method="POST">
                @csrf
                <input type="hidden" name="photoName" value="{{$photo->image}}">
                <button type="submit" class="btn btn-lg btn-danger">Remover</button>
             </form>
        </div>

    @endforeach
</div>

@endsection