<!-- extendo para o arquivo layouts.app, para poder substituir a section dentro dele-->

 @extends('layouts.app')

 <!-- O parametro dentro da diretiva section e onde irei adicionar -->
 @section('content')
 <a href="{{route('admin.products.create')}}" style="margin-bottom: 2%" class="btn btn-lg btn-success">Criar Produto</a>
 <!-- irei adicionar dentro de content-->
    <table class="table table-striped table-hover">
      <thead>
          <tr>
              <th>#</th>
              <th>Nome</th>
              <th>Preco</th>
              <th>Loja</th>
              <th>Acoes</th>
          </tr>
      </thead>
      <tbody>
        @foreach($products as $p)
        <tr>
            <td>{{$p->id}}</td>
            <td>{{$p->name}}</td>
            <td>R$ {{number_format($p->price, 2 , "," , ".")}}</td>
            <td>{{$p->store->name}}</td>

            <td>
          
            <div class="btn-group">
              <a href="{{route('admin.products.edit', ['product' => $p->id])}}" 
                class="btn btn-sm btn-primary">Editar</a>
            </div>

            <div class="btn-group">
              <form action="{{route('admin.products.destroy', ['product' => $p->id])}}" method="post">
                @csrf
                @method("delete")
                <button type="submit" class="btn btn-sm btn-danger">Remover</button>
              </form>
            </div>
            

            </td>
          </tr>
        @endforeach
      </tbody>
    </table>

  <!-- O metodo links vem do retorno de stores e traz o link paginate  -->
  {{$products->links()}}

  @endsection

