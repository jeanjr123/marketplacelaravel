<!-- coloco a diretiva extends para colocar o conteudo dentro de yield 
vou substituir dentro de layouts/app -->
@extends('layouts.app')
    
 <!-- O parametro dentro da diretiva section e onde irei adicionar -->
@section('content')

    <h1>Criar Produto</h1>
        <!-- esse route = uma funcao que espera o alias ou seja o apelido da rota definido
            com name no web dentro de routees, quando chamo o create esse form vai submeter
        para store onde e criado o produto =  store = nome do metodo de criacao -->  
<form action="{{route('admin.products.store')}}" method="post" enctype="multipart/form-data">
    <!-- O laravel precisa saber que a requisicao vem da propria aplicacao por isso
    uso o csrf token  -->
   <!-- <input type="hidden" name="_token" value="{{csrf_token()}}"> -->
     @csrf

        <div class="form-group">
            <label>Nome Produto</label>
            <input type="text" name="name" class="form-control 
            @error('name') is-invalid @enderror" value="{{old('name')}}" >
            @error('name')
              <div class="invalid-feedback">
                  {{$message}}
              </div>
            @enderror
        </div>
        
        <div class="form-group">
            <label>Descricao</label>
            <input type="text" name="description" class="form-control 
            @error('description') is-invalid @enderror" value="{{old('description')}}">

            @error('description')
            <div class="invalid-feedback">
                {{$message}}
            </div>
            @enderror
        </div>

        <div class="form-group">
            <label>Conteudo</label>
            <textarea name="body" id="" cols="30" rows="10" class="form-control
            @error('body') is-invalid @enderror" >{{old('body')}}</textarea>
            @error('body')
            <div class="invalid-feedback">
                {{$message}}
            </div>
            @enderror
        </div>
        
        <div class="form-group">
            <label>Preco</label>
            <input type="text" name="price" class="form-control 
            @error('price') is-invalid @enderror" value="{{old('price')}}">
            @error('price')
            <div class="invalid-feedback">
                {{$message}}
            </div>
            @enderror
        </div>

        <div class="form-group">
            <label>Categorias</label>
             <!-- Name = categories com notacao de array  -->
            <select name="categories[]" id="" 
            class="form-control @error('categories.*') is-invalid @enderror" multiple>
                @foreach ($categories as $category)
                     <option @if (old("categories")){{ (in_array($category->id, old("categories")) ? "selected":"") }}@endif  value="{{$category->id}}">{{$category->name}}</option>
                @endforeach
            </select>
            @error('categories.*')  
            <div class="invalid-feedback">
                {{$message}}
            </div>
            @enderror
        </div>

        <div class="form-group">
            <label>Fotos do produto</label>
            <input type="file" name="photos[]" 
            class="form-control @error('photos.*') is-invalid  @enderror" multiple>
            @error('photos.*')
            <div class="invalid-feedback">
               {{$message}}
            </div>
            @enderror
        </div>   
        
        <div>
            <button class="btn btn-lg btn-success" type="submit">Criar Produto</button>
        </div>


    </form>


@endsection