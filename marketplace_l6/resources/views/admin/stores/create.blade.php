<!-- coloco a diretiva extends para colocar o conteudo dentro de yield 
vou substituir dentro de layouts/app -->
@extends('layouts.app')
    
 <!-- O parametro dentro da diretiva section e onde irei adicionar -->
@section('content')

    <h1>Criar Loja</h1>
        <!-- esse route = uma funcao que espera o alias ou seja o apelido da rota definido
            com name no web dentro de routees, quando chamo o create esse form vai submeter
        para store onde e criado a loja -->
<form action="{{route('admin.stores.store')}}" method="post" enctype="multipart/form-data">
    <!-- O laravel precisa saber que a requisicao vem da propria aplicacao por isso
    uso o csrf token  -->
    <input type="hidden" name="_token" value="{{csrf_token()}}">

        <div class="form-group">
            <label>Nome Loja</label>
            <input type="text" name="name" class="form-control @error('name') is-invalid  @enderror " value="{{old('name')}}">
            <!-- a diretiva error valida se tem algum erro nesse campo de acordo com
                 o storeRequest posso personalizar uma mensagem ou passar message que 
                 pode ser a mensagem da diretiva -->
            @error('name')
                <div class="invalid-feedback">
                    {{$message}}
                </div>
            @enderror
        </div>
        
        <div class="form-group">
            <label>Descricao</label>
            <input type="text" name="description" class="form-control 
            @error('description') is-invalid  @enderror" value="{{old('description')}}" >
            @error('description')
                <div class="invalid-feedback">
                    {{$message}}
                </div>
            @enderror
        </div>
        
        <div class="form-group">
            <label>Telefone</label>
            <input type="text" name="phone" class="form-control 
            @error('phone') is-invalid  @enderror" value="{{old('phone')}}">
            @error('phone')
                <div class="invalid-feedback">
                    {{$message}}
                </div>
            @enderror
        
        </div>
        
        <div class="form-group">
            <label>Celular/Whatsapp</label>
            <input type="text" name="mobile_phone" class="form-control 
            @error('mobile_phone') is-invalid  @enderror" value="{{old('mobile_phone')}}">
            @error('mobile_phone')
                <div class="invalid-feedback">
                    {{$message}}
                </div>
            @enderror
        
        </div>

        <div class="form-group">
            <label>Fotos do produto</label>
            <input type="file" name="logo" class="form-control @error('logo') is-invalid  @enderror">
            @error('logo')
            <div class="invalid-feedback">
                {{$message}}
            </div>
            @enderror
        </div>
        
        
        {{-- <div class="form-group">
            <label>Usuario</label>
            <select type="text" name="user" class="form-control">
                @foreach($users as $user)
            <option value="{{$user->id}}">{{$user->name}}</option>
                @endforeach
            </select>
        </div> --}}
        
        <div>
            <button class="btn btn-lg btn-success" type="submit">Criar Loja</button>
        </div>
    </form>


@endsection