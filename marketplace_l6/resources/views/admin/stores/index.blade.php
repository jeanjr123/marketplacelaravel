<!-- extendo para o arquivo layouts.app, para poder substituir a section dentro dele-->

 @extends('layouts.app')

 <!-- O parametro dentro da diretiva section e onde irei adicionar -->
 @section('content')

 @if(!$store)
    <a href="{{route('admin.stores.create')}}" style="margin-bottom: 2%" class="btn btn-lg btn-success">Criar Loja</a>
 @else
    <!-- irei adicionar dentro de content-->
    <table class="table table-striped">
      <thead>
          <tr>
              <th>#</th>
              <th>Loja</th>
              <th>Total de Produtos</th>
              <th>Acoes</th>
          </tr>
      </thead>
      <tbody>
        <tr>
            <td>{{$store->id}}</td>
            <td>{{$store->name}}</td>
            <td>{{$store->products->count()}}</td>
            <td>


              <div class="btn-group">
                <a href="{{route('admin.stores.edit', ['store' => $store->id])}}" 
                  class="btn btn-sm btn-primary">Editar</a>
              </div>
                
              <div class="btn-group">
                <form action="{{route('admin.stores.destroy', ['store' => $store->id  ])}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button type="submit" class="btn btn-sm btn-danger">Remover</button>
                </form>
              </div>
            </td>
          </tr>
    
      </tbody>
    </table>
    @endif
  <!-- O metodo links vem do retorno de stores e traz o link paginate  -->
  {{-- {{$store->links()}} --}}

  @endsection

