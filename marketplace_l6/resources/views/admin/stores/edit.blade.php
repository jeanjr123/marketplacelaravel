<!-- coloco a diretiva extends para colocar o conteudo dentro de yield 
vou substituir dentro de layouts/app -->
@extends('layouts.app')
    
 <!-- O parametro dentro da diretiva section e onde irei adicionar -->
@section('content')

    <h1>Criar Loja</h1>
                          <!-- o segundo parametro de route = array associativo
                            com o nome do parametro dinamico e o valor -->
    <form action="{{route('admin.stores.update', [ 'store' => $store->id ])}}" method="post" enctype="multipart/form-data">
    @csrf
    @method('put')
        <div class="form-group">
            <label>Nome Loja</label>
        <input type="text" name="name" class="form-control" value="{{$store->name}}">
        </div>
        
        <div class="form-group">
            <label>Descricao</label>
            <input type="text" name="description" class="form-control" value="{{$store->description}}">
        </div>
        
        <div class="form-group">
            <label>Telefone</label>
            <input type="text" name="phone" class="form-control" value="{{$store->phone}}">
        </div>
        
        <div class="form-group">
            <label>Celular/Whatsapp</label>
            <input type="text" name="mobile_phone" class="form-control" value="{{$store->mobile_phone}}">
        </div>

        <div class="form-group">
            <p>
            <img src="{{asset('storage/' . $store->logo)}}" alt="">
            </p>
            <label>Fotos do produto</label>
            <input type="file" name="logo" class="form-control @error('logo') is-invalid  @enderror">
            @error('logo')
            <div class="invalid-feedback">
                {{$message}}
            </div>
            @enderror
        </div>
       
        <div>
            <button class="btn btn-lg btn-success" type="submit">Atualizar Loja</button>
        </div>


    </form>


@endsection